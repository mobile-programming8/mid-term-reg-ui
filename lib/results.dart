import 'package:flutter/material.dart';
import 'schedule.dart';
import 'profile.dart';
import 'regapp.dart';
import 'login.dart';

class ResultsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Reg App',
      home: ResultsPageScreen(),
    );
  }
}

class ResultsPageScreen extends StatefulWidget {
  @override
  _ResultsPageScreen createState() => _ResultsPageScreen();
}

class _ResultsPageScreen extends State<ResultsPageScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text("Study Results"),
          backgroundColor: Colors.purple,
      ),
      drawer: NavigationDrawer(context),
      body: SingleChildScrollView(
        child: Container(
            padding: EdgeInsets.all(10),
            child: Column(
              children: <Widget>[
                Table(
                  border: TableBorder.all(),
                  defaultColumnWidth: const FixedColumnWidth(370),
                  columnWidths: {
                    0: FractionColumnWidth(1),
                  },
                  children: <TableRow>[
                    TableRow(
                        decoration: BoxDecoration(
                          color: Colors.purple
                        ),
                        children: <Widget>[
                          Center(
                            child: Text('ภาคการศึกษาที่ 1/2563',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 15,
                              color: Colors.white
                            ),),
                          )
                        ]
                    ),
                  ],
                ),
                Table(
                  border: TableBorder.all(),
                  defaultColumnWidth: const FixedColumnWidth(92.5),
                  columnWidths: {
                    0: FractionColumnWidth(0.2),
                    1: FractionColumnWidth(0.5),
                    2: FractionColumnWidth(0.15),
                    3: FractionColumnWidth(0.15),
                  },
                  children: <TableRow>[
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('รหัสวิชา',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            )),
                          ),
                          Center(
                            child: Text('ชื่อรายวิชา',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                          Center(
                            child: Text('หน่วยกิต',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                          Center(
                            child: Text('เกรด',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('30910159'),
                          ),
                          Center(
                            child: Text('Marine Ecology and Ecotourism'),
                          ),
                          Center(
                            child: Text('2'),
                          ),
                          Center(
                            child: Text('B+'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('40240359'),
                          ),
                          Center(
                            child: Text('Sufficiency Economy and Social Development'),
                          ),
                          Center(
                            child: Text('2'),
                          ),
                          Center(
                            child: Text('B'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('85111059'),
                          ),
                          Center(
                            child: Text('Exercise for Quality of Life'),
                          ),
                          Center(
                            child: Text('2'),
                          ),
                          Center(
                            child: Text('A'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88510059'),
                          ),
                          Center(
                            child: Text('Logical Thinking and Problem Solving for Innovation'),
                          ),
                          Center(
                            child: Text('2'),
                          ),
                          Center(
                            child: Text('A'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88510159'),
                          ),
                          Center(
                            child: Text('Moving Forward in a Digital Society with ICT'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('A'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88510259'),
                          ),
                          Center(
                            child: Text('Discrete Structures'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('A'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('99910259'),
                          ),
                          Center(
                            child: Text('Collegiate English'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('A'),
                          )
                        ]
                    ),
                  ],
                ),
                Container(
                  padding: EdgeInsets.all(10),
                ),
                Table(
                  border: TableBorder.all(),
                  defaultColumnWidth: const FixedColumnWidth(370),
                  columnWidths: {
                    0: FractionColumnWidth(1),
                  },
                  children: <TableRow>[
                    TableRow(
                        decoration: BoxDecoration(
                            color: Colors.purple
                        ),
                        children: <Widget>[
                          Center(
                            child: Text('ภาคการศึกษาที่ 2/2563',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                  color: Colors.white
                              ),),
                          )
                        ]
                    ),
                  ],
                ),
                Table(
                  border: TableBorder.all(),
                  defaultColumnWidth: const FixedColumnWidth(92.5),
                  columnWidths: {
                    0: FractionColumnWidth(0.2),
                    1: FractionColumnWidth(0.5),
                    2: FractionColumnWidth(0.15),
                    3: FractionColumnWidth(0.15),
                  },
                  children: <TableRow>[
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('รหัสวิชา',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                          Center(
                            child: Text('ชื่อรายวิชา',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                          Center(
                            child: Text('หน่วยกิต',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                          Center(
                            child: Text('เกรด',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('61010159'),
                          ),
                          Center(
                            child: Text('Art and Life'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('B'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('77037959'),
                          ),
                          Center(
                            child: Text('Arts and Creativity'),
                          ),
                          Center(
                            child: Text('2'),
                          ),
                          Center(
                            child: Text('A'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88510359'),
                          ),
                          Center(
                            child: Text('Mathematics for Computing'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('A'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88510459'),
                          ),
                          Center(
                            child: Text('Programming Fundamental'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('A'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88612159'),
                          ),
                          Center(
                            child: Text('Introduction to Computer Science'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('A'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('99920159'),
                          ),
                          Center(
                            child: Text('English Writing for Communication'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('C+'),
                          )
                        ]
                    )
                  ],
                ),
                Container(
                  padding: EdgeInsets.all(10),
                ),
                Table(
                  border: TableBorder.all(),
                  defaultColumnWidth: const FixedColumnWidth(370),
                  columnWidths: {
                    0: FractionColumnWidth(1),
                  },
                  children: <TableRow>[
                    TableRow(
                        decoration: BoxDecoration(
                            color: Colors.purple
                        ),
                        children: <Widget>[
                          Center(
                            child: Text('ภาคการศึกษาที่ 1/2564',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                  color: Colors.white
                              ),),
                          )
                        ]
                    ),
                  ],
                ),
                Table(
                  border: TableBorder.all(),
                  defaultColumnWidth: const FixedColumnWidth(92.5),
                  columnWidths: {
                    0: FractionColumnWidth(0.2),
                    1: FractionColumnWidth(0.5),
                    2: FractionColumnWidth(0.15),
                    3: FractionColumnWidth(0.15),
                  },
                  children: <TableRow>[
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('รหัสวิชา',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                          Center(
                            child: Text('ชื่อรายวิชา',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                          Center(
                            child: Text('หน่วยกิต',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                          Center(
                            child: Text('เกรด',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('25710259'),
                          ),
                          Center(
                            child: Text('Economics of Everyday Life'),
                          ),
                          Center(
                            child: Text('2'),
                          ),
                          Center(
                            child: Text('B+'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('30211359'),
                          ),
                          Center(
                            child: Text('Calculus'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('A'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88520159'),
                          ),
                          Center(
                            child: Text('Probability and Statistics for Computing'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('A'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88520259'),
                          ),
                          Center(
                            child: Text('English for Informatics'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('B'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88620159'),
                          ),
                          Center(
                            child: Text('Object-Oriented Programming Paradigm'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('A'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88620259'),
                          ),
                          Center(
                            child: Text('Relational Database'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('A'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('99910159'),
                          ),
                          Center(
                            child: Text('English for Communication'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('A'),
                          )
                        ]
                    )
                  ],
                ),
                Container(
                  padding: EdgeInsets.all(10),
                ),
                Table(
                  border: TableBorder.all(),
                  defaultColumnWidth: const FixedColumnWidth(370),
                  columnWidths: {
                    0: FractionColumnWidth(1),
                  },
                  children: <TableRow>[
                    TableRow(
                        decoration: BoxDecoration(
                            color: Colors.purple
                        ),
                        children: <Widget>[
                          Center(
                            child: Text('ภาคการศึกษาที่ 2/2564',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                  color: Colors.white
                              ),),
                          )
                        ]
                    ),
                  ],
                ),
                Table(
                  border: TableBorder.all(),
                  defaultColumnWidth: const FixedColumnWidth(92.5),
                  columnWidths: {
                    0: FractionColumnWidth(0.2),
                    1: FractionColumnWidth(0.5),
                    2: FractionColumnWidth(0.15),
                    3: FractionColumnWidth(0.15),
                  },
                  children: <TableRow>[
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('รหัสวิชา',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                          Center(
                            child: Text('ชื่อรายวิชา',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                          Center(
                            child: Text('หน่วยกิต',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                          Center(
                            child: Text('เกรด',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88620359'),
                          ),
                          Center(
                            child: Text('Non-Relational Database'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('A'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88620459'),
                          ),
                          Center(
                            child: Text('Introduction to Data Science and Data Analytics'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('A'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88621159'),
                          ),
                          Center(
                            child: Text('Data Structures and Algorithms'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('A'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88622259'),
                          ),
                          Center(
                            child: Text('Operating Systems'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('A'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88624159'),
                          ),
                          Center(
                            child: Text('Unix Tools and Programming'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('A'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('99940359'),
                          ),
                          Center(
                            child: Text('Portuguese for Communication'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('B+'),
                          )
                        ]
                    )
                  ],
                ),
                Container(
                  padding: EdgeInsets.all(10),
                ),
                Table(
                  border: TableBorder.all(),
                  defaultColumnWidth: const FixedColumnWidth(370),
                  columnWidths: {
                    0: FractionColumnWidth(1),
                  },
                  children: <TableRow>[
                    TableRow(
                        decoration: BoxDecoration(
                            color: Colors.purple
                        ),
                        children: <Widget>[
                          Center(
                            child: Text('ภาคการศึกษาที่ 1/2565',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                  color: Colors.white
                              ),),
                          )
                        ]
                    ),
                  ],
                ),
                Table(
                  border: TableBorder.all(),
                  defaultColumnWidth: const FixedColumnWidth(92.5),
                  columnWidths: {
                    0: FractionColumnWidth(0.2),
                    1: FractionColumnWidth(0.5),
                    2: FractionColumnWidth(0.15),
                    3: FractionColumnWidth(0.15),
                  },
                  children: <TableRow>[
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('รหัสวิชา',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                          Center(
                            child: Text('ชื่อรายวิชา',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                          Center(
                            child: Text('หน่วยกิต',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                          Center(
                            child: Text('เกรด',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88624259'),
                          ),
                          Center(
                            child: Text('Mobile Programming Paradigm'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('B+'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88631159'),
                          ),
                          Center(
                            child: Text('Algorithm Design and Applications'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('C'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88633159'),
                          ),
                          Center(
                            child: Text('Computer Networks'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('B'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88634159'),
                          ),
                          Center(
                            child: Text('Software Development'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('A'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88635359'),
                          ),
                          Center(
                            child: Text('User Interface Design and Development'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('A'),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88636159'),
                          ),
                          Center(
                            child: Text('Introduction to Artificial Intelligence'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text('B+'),
                          )
                        ]
                    )
                  ],
                ),
                Container(
                  padding: EdgeInsets.all(10),
                ),
                Table(
                  border: TableBorder.all(),
                  defaultColumnWidth: const FixedColumnWidth(370),
                  columnWidths: {
                    0: FractionColumnWidth(1),
                  },
                  children: <TableRow>[
                    TableRow(
                        decoration: BoxDecoration(
                            color: Colors.purple
                        ),
                        children: <Widget>[
                          Center(
                            child: Text('ภาคการศึกษาที่ 2/2565',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                  color: Colors.white
                              ),),
                          )
                        ]
                    ),
                  ],
                ),
                Table(
                  border: TableBorder.all(),
                  defaultColumnWidth: const FixedColumnWidth(92.5),
                  columnWidths: {
                    0: FractionColumnWidth(0.2),
                    1: FractionColumnWidth(0.5),
                    2: FractionColumnWidth(0.15),
                    3: FractionColumnWidth(0.15),
                  },
                  children: <TableRow>[
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('รหัสวิชา',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                          Center(
                            child: Text('ชื่อรายวิชา',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                          Center(
                            child: Text('หน่วยกิต',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          ),
                          Center(
                            child: Text('เกรด',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                )),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('23629164'),
                          ),
                          Center(
                            child: Text('Japanese for Communication'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text(''),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88624359'),
                          ),
                          Center(
                            child: Text('Web Programming'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text(''),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88624459'),
                          ),
                          Center(
                            child: Text('Object-Oriented Analysis and Design'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text(''),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88624559'),
                          ),
                          Center(
                            child: Text('Software Testing'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text(''),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88634259'),
                          ),
                          Center(
                            child: Text('Multimedia Programming for Multiplatforms'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text(''),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88634459'),
                          ),
                          Center(
                            child: Text('Mobile Application Development I'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text(''),
                          )
                        ]
                    ),
                    TableRow(
                        children: <Widget>[
                          Center(
                            child: Text('88646259'),
                          ),
                          Center(
                            child: Text('Introduction to Natural Language Processing'),
                          ),
                          Center(
                            child: Text('3'),
                          ),
                          Center(
                            child: Text(''),
                          )
                        ]
                    )
                  ],
                ),
              ],
            )
        ),
      )
    );
  }

  Drawer NavigationDrawer(BuildContext context) {
    return Drawer(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            buildHeader(context),
            buildMenuItems(context),
          ],
        ),
      ),
    );
  }

  Widget buildHeader(BuildContext context) => Container(
    padding: EdgeInsets.only(
        top: 55
    ),
  );

  Widget buildMenuItems(BuildContext context) => Container(
    padding: const EdgeInsets.all(24),
    child: Wrap(
      runSpacing: 16,
      children: [
        ListTile(
          leading: const Icon(Icons.home_outlined),
          title: const Text('Home'),
          onTap: () =>
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => RegApp(),
              )),
        ),Divider(
          thickness: 1,
        ),ListTile(
          leading: const Icon(Icons.schedule_outlined),
          title: const Text('Schedule'),
          onTap: () =>
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => SchedulePage(),
              )),
        ),ListTile(
          leading: const Icon(Icons.book_outlined),
          title: const Text('Study Results'),
          onTap: () {},
        ),ListTile(
          leading: const Icon(Icons.person),
          title: const Text('Student Profile'),
          onTap: () =>
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => ProfilePage(),
              )),
        ),Divider(
          thickness: 1,
        ),
        ListTile(
            leading: const Icon(Icons.logout_outlined),
            title: const Text('Log out'),
            onTap: () {
              Navigator.pop(context);
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => const loginScreen(),
              ));
            }
        )
      ],
    ),
  );
}