import 'package:flutter/material.dart';
import 'schedule.dart';
import 'results.dart';
import 'regapp.dart';
import 'login.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Reg App',
      home: ProfilePageScreen(),
    );
  }
}

class ProfilePageScreen extends StatefulWidget {
  @override
  _ProfilePageScreen createState() => _ProfilePageScreen();
}

class _ProfilePageScreen extends State<ProfilePageScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text("Profile"),
          backgroundColor: Colors.purple,
      ),
      drawer: NavigationDrawer(context),
      body: SingleChildScrollView(
          child: Container(
              padding: EdgeInsets.all(10),
              child: Column(
                children: <Widget>[
                  Table(
                    border: TableBorder.all(),
                    defaultColumnWidth: const FixedColumnWidth(370),
                    columnWidths: {
                      0: FractionColumnWidth(1),
                    },
                    children: <TableRow>[
                      TableRow(
                          decoration: BoxDecoration(
                              color: Colors.purple
                          ),
                          children: <Widget>[
                            Text('ข้อมูลด้านการศึกษา',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15,
                                    color: Colors.white
                                ),
                            ),
                          ]
                      ),
                    ],
                  ),
                  Table(
                    border: TableBorder.all(),
                    defaultColumnWidth: const FixedColumnWidth(92.5),
                    columnWidths: {
                      0: FractionColumnWidth(0.4),
                      1: FractionColumnWidth(0.6),
                    },
                    children: <TableRow>[
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('รหัสประจำตัว:'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('63160009'),
                            ),
                          ]
                      ),
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('เลขที่บัตรประชาชน:'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('1300101242943'),
                            ),
                          ]
                      ),
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('ชื่อ:'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('นายภาดล วงษ์ศิริ'),
                            ),
                          ]
                      ),
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('ชื่ออังกฤษ:'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('MR. PHADOL WONGSIRI'),
                            ),
                          ]
                      ),
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('คณะ:'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('คณะวิทยาการสารสนเทศ'),
                            ),
                          ]
                      ),
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('วิทยาเขต:'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('บางแสน'),
                            ),
                          ]
                      ),
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('หลักสูตร:'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('2115020 วท.บ. (วิทยาการคอมพิวเตอร์) ปรับปรุง 59 - ป.ตรี 4 ปี ปกติ'),
                            ),
                          ]
                      ),
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('ปีการศึกษาที่เข้า:'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('2563 / 1 \nวันที่ 5/2/2563'),
                            ),
                          ]
                      ),
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('สถานภาพ:'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text(''),
                            ),
                          ]
                      ),
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('วิธีรับเข้า:'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('โครงการความร่วมมือทางวิชาการ (MOU)'),
                            ),
                          ]
                      ),
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('วุฒิก่อนเข้ารับการศึกษา:'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('ม.6'),
                            ),
                          ]
                      ),
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('จบการศึกษาจาก:'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('ชลราษฎรอำรุง'),
                            ),
                          ]
                      ),
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('อ. ที่ปรึกษา:'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('ผู้ช่วยศาสตราจารย์ ดร.โกเมศ อัมพวัน,อาจารย์ภูสิต กุลเกษม'),
                            ),
                          ]
                      )
                    ],
                  ),
                  Table(
                    border: TableBorder.all(),
                    defaultColumnWidth: const FixedColumnWidth(370),
                    columnWidths: {
                      0: FractionColumnWidth(1),
                    },
                    children: <TableRow>[
                      TableRow(
                          decoration: BoxDecoration(
                              color: Colors.purple
                          ),
                          children: <Widget>[
                            Text('ผลการศึกษา',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                  color: Colors.white
                              ),
                            ),
                          ]
                      ),
                    ],
                  ),
                  Table(
                    border: TableBorder.all(),
                    defaultColumnWidth: const FixedColumnWidth(92.5),
                    columnWidths: {
                      0: FractionColumnWidth(0.4),
                      1: FractionColumnWidth(0.6),
                    },
                    children: <TableRow>[
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('หน่วยกิตคำนวณ:'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('90'),
                            ),
                          ]
                      ),
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('หน่วยกิตที่ผ่าน:'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('90'),
                            ),
                          ]
                      ),
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('คะแนนเฉลี่ยสะสม:'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('3.69'),
                            ),
                          ]
                      )
                    ],
                  ),
                  Table(
                    border: TableBorder.all(),
                    defaultColumnWidth: const FixedColumnWidth(370),
                    columnWidths: {
                      0: FractionColumnWidth(1),
                    },
                    children: <TableRow>[
                      TableRow(
                          decoration: BoxDecoration(
                              color: Colors.purple
                          ),
                          children: <Widget>[
                            Text('ข้อมูลส่วนบุคคล',
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                  color: Colors.white
                              ),
                            ),
                          ]
                      ),
                    ],
                  ),
                  Table(
                    border: TableBorder.all(),
                    defaultColumnWidth: const FixedColumnWidth(92.5),
                    columnWidths: {
                      0: FractionColumnWidth(0.4),
                      1: FractionColumnWidth(0.6),
                    },
                    children: <TableRow>[
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('สัญชาติ:'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('ไทย '),
                            ),
                          ]
                      ),
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('ศาสนา:'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('พุทธ'),
                            ),
                          ]
                      ),
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('หมู่เลือด:'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('O'),
                            ),
                          ]
                      ),
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('ชื่อบิดา:'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('ปกรณ์ วงษ์ศิริ'),
                            ),
                          ]
                      ),
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('ชื่อมารดา:'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('ชื่อมารดา:'),
                            ),
                          ]
                      ),
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('ที่อยู่'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('"-'
                                  '\nเหมือง'
                                  '\nเขต/อำเภอ เมืองชลบุรี'
                                  '\nชลบุรี 20130'
                                  '\nโทร: 0982500512"'),
                            ),
                          ]
                      ),
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('ผู้ปกครอง:'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('สุภาวดี วิชาเรือง'),
                            ),
                          ]
                      ),
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('ความสัมพันธ์:'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('3'),
                            ),
                          ]
                      ),
                      TableRow(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('ที่อยู่ ( ผู้ปกครอง )'),
                            ),
                            Container(
                              padding: EdgeInsets.all(2),
                              child: Text('\nเหมือง'
                                  '\nเขต/อำเภอ เมืองชลบุรี'
                                  '\nชลบุรี 20130'
                                  '\nโทร: 0819062317'),
                            ),
                          ]
                      ),
                    ],
                  ),
                ],
              )
          ),
        )
    );
  }

  Drawer NavigationDrawer(BuildContext context) {
    return Drawer(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            buildHeader(context),
            buildMenuItems(context),
          ],
        ),
      ),
    );
  }

  Widget buildHeader(BuildContext context) => Container(
    padding: EdgeInsets.only(
        top: 55
    ),
  );

  Widget buildMenuItems(BuildContext context) => Container(
    padding: const EdgeInsets.all(24),
    child: Wrap(
      runSpacing: 16,
      children: [
        ListTile(
          leading: const Icon(Icons.home_outlined),
          title: const Text('Home'),
          onTap: () =>
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => RegApp(),
              )),
        ),Divider(
          thickness: 1,
        ),ListTile(
          leading: const Icon(Icons.schedule_outlined),
          title: const Text('Schedule'),
          onTap: () =>
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => SchedulePage(),
              )),
        ),ListTile(
          leading: const Icon(Icons.book_outlined),
          title: const Text('Study Results'),
          onTap: () =>
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => ResultsPage(),
              )),
        ),ListTile(
          leading: const Icon(Icons.person),
          title: const Text('Student Profile'),
          onTap: () {},
        ),Divider(
          thickness: 1,
        ),
        ListTile(
            leading: const Icon(Icons.logout_outlined),
            title: const Text('Log out'),
            onTap: () {
              Navigator.pop(context);
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => const loginScreen(),
              ));
            }
        )
      ],
    ),
  );
}