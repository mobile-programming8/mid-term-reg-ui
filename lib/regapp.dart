import 'package:flutter/material.dart';
import 'login.dart';
import 'profile.dart';
import 'schedule.dart';
import 'results.dart';

void main() {
  runApp(RegApp());
}

class RegApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Reg App',
      home: RegScreen(),
    );
  }
}

class RegScreen extends StatefulWidget {
  @override
  _RegScreenState createState() => _RegScreenState();
}

class _RegScreenState extends State<RegScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: const Text("Reg"),
            backgroundColor: Colors.purple,
        ),
        drawer: NavigationDrawer(context),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(10),
            child: Column(
              children: <Widget>[
                Text("ยินดีต้อนรับเข้าสู่ระบบบริการการศึกษา",
                  style: TextStyle(
                    color: Colors.orange,
                    fontSize: MediaQuery.of(context).size.width * 0.025,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text("ประกาศเรื่อง",
                  style: TextStyle(
                    color: Colors.redAccent,
                    fontSize: MediaQuery.of(context).size.width * 0.02,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Wrap(
                    children:[
                      Text("1. แบบประเมินความคิดเห็นของนักเรียนและนิสิตต่อการให้บริการของสำนักงานอธิการบดี(ด่วนที่สุด)"
                          "\nขอเชิญนิสิตร่วมทำแบบประเมินความคิดเห็นของนิสิตต่อการให้บริการของสำนักงานอธิการบดี ที่",
                          style: TextStyle(
                            fontSize: MediaQuery.of(context).size.width * 0.015,
                          ),
                      ),
                      Text("https://bit.ly/3cyvuuf",
                        style: TextStyle(
                          color: Colors.blue,
                          decoration: TextDecoration.underline,
                          fontSize: MediaQuery.of(context).size.width * 0.015,
                        ),
                      ),
                      Text("2. การทำบัตรนิสิตกับธนาคารกรุงไทย"
                          "\nกรณีบัตรหายเสียค่าใช้จ่ายในการทำ 100 บาท"
                          "\nสำหรับนิสิตรหัส 65 วิทยาเขตบางแสนที่เข้าภาคเรียนที่ 1 ที่ยังไม่รับบัตรให้ติดต่อรับบัตรนิสิตที่ธนาคารกรุงไทย สาขา ม.บูรพา \nส่วนนิสิตที่เข้าภาคเรียนที่ 2/2565 รอกำหนดการอีกครั้ง",
                        style: TextStyle(
                          fontSize: MediaQuery.of(context).size.width * 0.015,
                        ),
                      ),
                    ]
                ),
                Container(
                  padding: EdgeInsets.all(10),
                )
                ,
                Image(
                    image: NetworkImage('https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Buu-logo11.png/1024px-Buu-logo11.png'),
                  width: MediaQuery.of(context).size.width * 0.15,
                )
              ],
            ),
          )
        ),
    );
  }

  Drawer NavigationDrawer(BuildContext context) {
    return Drawer(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              buildHeader(context),
              buildMenuItems(context),
            ],
          ),
        ),
      );
  }

  Widget buildHeader(BuildContext context) => Container(
    padding: EdgeInsets.only(
      top: 55
    ),
  );

  Widget buildMenuItems(BuildContext context) => Container(
    padding: const EdgeInsets.all(24),
    child: Wrap(
      runSpacing: 16,
      children: [
        ListTile(
          leading: const Icon(Icons.home_outlined),
          title: const Text('Home'),
          onTap: () {},
        ),Divider(
          thickness: 1,
        ),ListTile(
          leading: const Icon(Icons.schedule_outlined),
          title: const Text('Schedule'),
          onTap: () =>
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => SchedulePage(),
              )),
        ),ListTile(
          leading: const Icon(Icons.book_outlined),
          title: const Text('Study Results'),
          onTap: () =>
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => ResultsPage(),
              )),
        ),ListTile(
          leading: const Icon(Icons.person),
          title: const Text('Student Profile'),
          onTap: () =>
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => ProfilePage(),
              )),
        ),Divider(
          thickness: 1,
        ),
        ListTile(
          leading: const Icon(Icons.logout_outlined),
          title: const Text('Log out'),
          onTap: () {
              Navigator.pop(context);
              Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => const loginScreen(),
            ));
          }
        )
      ],
    ),
  );
}