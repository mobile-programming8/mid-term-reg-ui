import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'regapp.dart';
import 'results.dart';
import 'profile.dart';
import 'login.dart';

class SchedulePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Reg App',
      home: SchedulePageScreen(),
    );
  }
}

class SchedulePageScreen extends StatefulWidget {
  @override
  _SchedulePageScreen createState() => _SchedulePageScreen();
}

class _SchedulePageScreen extends State<SchedulePageScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text("Schedule"),
          backgroundColor: Colors.purple,
      ),
      drawer: NavigationDrawer(context),
      body: SfCalendar(
        todayHighlightColor: Colors.purple,
        view: CalendarView.week,
        dataSource: StudyDataSource(getAppointments()),
      ),
    );
  }

  Drawer NavigationDrawer(BuildContext context) {
    return Drawer(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            buildHeader(context),
            buildMenuItems(context),
          ],
        ),
      ),
    );
  }

  Widget buildHeader(BuildContext context) => Container(
    padding: EdgeInsets.only(
        top: 55
    ),
  );

  Widget buildMenuItems(BuildContext context) => Container(
    padding: const EdgeInsets.all(24),
    child: Wrap(
      runSpacing: 16,
      children: [
        ListTile(
          leading: const Icon(Icons.home_outlined),
          title: const Text('Home'),
          onTap: () =>
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => RegApp(),
              )),
        ),Divider(
          thickness: 1,
        ),ListTile(
          leading: const Icon(Icons.schedule_outlined),
          title: const Text('Schedule'),
          onTap: () {},
        ),ListTile(
          leading: const Icon(Icons.book_outlined),
          title: const Text('Study Results'),
          onTap: () =>
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => ResultsPage(),
              )),
        ),ListTile(
          leading: const Icon(Icons.person),
          title: const Text('Student Profile'),
          onTap: () =>
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => ProfilePage(),
              )),
        ),Divider(
          thickness: 1,
        ),
        ListTile(
            leading: const Icon(Icons.logout_outlined),
            title: const Text('Log out'),
            onTap: () {
              Navigator.pop(context);
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => const loginScreen(),
              ));
            }
        )
      ],
    ),
  );
}

List<Appointment> getAppointments() {
  List<Appointment> studies = <Appointment>[];
  DateTime day = DateTime.now();

  studies.add(Appointment(
    startTime: DateTime(day.year, day.month, day.day, 10, 0, 0),
    endTime: DateTime(day.year, day.month, day.day, 12, 0, 0),
    subject: 'Mobile Programing',
    color: Colors.purple
  ));

  studies.add(Appointment(
      startTime: DateTime(day.year, day.month, day.day, 13, 0, 0),
      endTime: DateTime(day.year, day.month, day.day, 16, 0, 0),
      subject: 'NLP',
      color: Colors.purple
  ));

  studies.add(Appointment(
      startTime: DateTime(day.year, day.month, day.day, 17, 0, 0),
      endTime: DateTime(day.year, day.month, day.day, 19, 0, 0),
      subject: 'Web Programing',
      color: Colors.purple
  ));

  DateTime tmr = day.add(const Duration(days: 1));

  studies.add(Appointment(
      startTime: DateTime(tmr.year, tmr.month, tmr.day, 9, 0, 0),
      endTime: DateTime(tmr.year, tmr.month, tmr.day, 12, 0, 0),
      subject: 'Japanese',
      color: Colors.purple
  ));

  DateTime tmr2 = day.add(const Duration(days: 2));

  studies.add(Appointment(
      startTime: DateTime(tmr2.year, tmr2.month, tmr2.day, 13, 0, 0),
      endTime: DateTime(tmr2.year, tmr2.month, tmr2.day, 15, 0, 0),
      subject: 'Multimedia Programing',
      color: Colors.purple
  ));

  studies.add(Appointment(
      startTime: DateTime(tmr2.year, tmr2.month, tmr2.day, 15, 0, 0),
      endTime: DateTime(tmr2.year, tmr2.month, tmr2.day, 17, 0, 0),
      subject: 'Mobile Programing',
      color: Colors.purple
  ));

  DateTime tmr3 = day.add(const Duration(days: 3));
  studies.add(Appointment(
      startTime: DateTime(tmr3.year, tmr3.month, tmr3.day, 10, 0, 0),
      endTime: DateTime(tmr3.year, tmr3.month, tmr3.day, 12, 0, 0),
      subject: 'Software Testing',
      color: Colors.purple
  ));

  studies.add(Appointment(
      startTime: DateTime(tmr3.year, tmr3.month, tmr3.day, 13, 0, 0),
      endTime: DateTime(tmr3.year, tmr3.month, tmr3.day, 15, 0, 0),
      subject: 'OOAD',
      color: Colors.purple
  ));

  studies.add(Appointment(
      startTime: DateTime(tmr3.year, tmr3.month, tmr3.day, 17, 0, 0),
      endTime: DateTime(tmr3.year, tmr3.month, tmr3.day, 19, 0, 0),
      subject: 'Web Programing',
      color: Colors.purple
  ));

  DateTime tmr4 = day.add(const Duration(days: -1));
  studies.add(Appointment(
      startTime: DateTime(tmr4.year, tmr4.month, tmr4.day, 10, 0, 0),
      endTime: DateTime(tmr4.year, tmr4.month, tmr4.day, 12, 0, 0),
      subject: 'Multimedia Programing',
      color: Colors.purple
  ));

  studies.add(Appointment(
      startTime: DateTime(tmr4.year, tmr4.month, tmr4.day, 13, 0, 0),
      endTime: DateTime(tmr4.year, tmr4.month, tmr4.day, 15, 0, 0),
      subject: 'OOAD',
      color: Colors.purple
  ));

  studies.add(Appointment(
      startTime: DateTime(tmr4.year, tmr4.month, tmr4.day, 15, 0, 0),
      endTime: DateTime(tmr4.year, tmr4.month, tmr4.day, 17, 0, 0),
      subject: 'Software Testing',
      color: Colors.purple
  ));

  return studies;
}

class StudyDataSource extends CalendarDataSource{
  StudyDataSource(List<Appointment> source){
    appointments = source;
  }
}